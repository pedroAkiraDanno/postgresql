------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
postgresql













------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SIZE

one character = 8 bits or one byte



bit = 0 or 1

byte = 8 bits

kilobytes = 1.000 bytes

megabytes = 1.000.000 bytes

gigabytes = 1000 MB (megabytes)






bytes to gigabytes = 1024 / 1024 / 1024

gigabytes to bytes = 1024 * 1024 * 1024





-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-09-2021

Hexadecimal


Base 16
0 1 2 3 4 5 6 7 8  9 A B C D E F

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


DISK

sectors commonly 512 bytes

(PostgreSQL typically writes 8192 bytes, or 16 sectors, at a time)














------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-09-2021


--Como iniciar no Banco de Dados - Primeiros passos - Aula 1_2 _ CreateSe



postgres@srv:~$ createdb -U postgres
psql -U postgres test1










------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-09-2021


log




select *
from pg_settings;


logs, no postgres, não registra alterações dos dados dos bancos, mas somente eventos relacioanados ao estado do servidor, tais como, startup, prontidão para conexoes e, principalmente, erros.

Veja um exemplo de log de um servidor em um sistema linux:

$ tail -100f /var/log/postgresql/postgresql-13-main.log

2017-07-10 23:50:49 BRT [1165-2] LOG: database system was not properly shut down; automatic recovery in progress
2017-07-10 23:50:50 BRT [1165-3] LOG: invalid record length at 0/5EBE9C8
2017-07-10 23:50:50 BRT [1165-4] LOG: redo is not required
2017-07-10 23:50:50 BRT [1165-5] LOG: MultiXact member wraparound protections are now enabled
2017-07-10 23:50:50 BRT [1091-1] LOG: database system is ready to accept connections
...








------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-09-2021

WAL Write-Ahead Logging


LSN (Log Sequence Number) of XLOG record represents the location where its record is written on the transaction log. LSN of record is used as the unique id


By the way, when we consider how database system recovers, there may be one question; what point does PostgreSQL start to recover from? The answer is REDO point; that is, the location to write the XLOG record at the moment when the latest checkpoint is started




WAL segment filename

hexadecimal 24-digit number

00000001000000010000000F


hex     decimal     bin
FF   	255         11111111



WAL Segment = file
WAL segment is a 16 MB file, by default
internally divided into pages of 8192 bytes (8 KB).

min_wal_size (by default, 80 MB, i.e. 5 files)
max_wal_size (the default value is 1GB (64 files))


SELECT pg_switch_wal();




SELECT pg_walfile_name(pg_switch_wal()), pg_walfile_name(pg_switch_wal());

https://pgpedia.info/p/pg_switch_wal.html


Switched file is usually recycled (renamed and reused) for future use but it may be removed later if not necessary.


SELECT COUNT(*) FROM pg_ls_dir('pg_wal');






The number of WAL segment files in pg_xlog directory depends on min_wal_size, max_wal_size and the amount of WAL generated in previous checkpoint cycles. When old log segment files are no longer needed, they are removed or recycled (that is, renamed to become future segments in the numbered sequence). If, due to a short-term peak of log output rate, max_wal_size is exceeded, the unneeded segment files will be removed until the system gets back under this limit. Below that limit, the system recycles enough WAL files to cover the estimated need until the next checkpoint, and removes the rest




127.0.0.1


127.0.0.1:52268/browser/
ALTER USER postgres PASSWORD 'newPassword';


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
archive


postgres=# show archive_mode;

postgres=# alter system set archive_mode to 'on';

sudo systemctl restart postgresql
systemctl restart postgresql.service

mkdir -p /bkp/archive
chown postgres:postgres /bkp/archive/

postgres=# alter system set archive_command to 'cp %p /bkp/archive/%f';

postgres=# select pg_reload_conf();





[root@localhost ~ ]# df -h
[root@localhost ~ ]# lsblk
[root@localhost ~ ]# ls -l /dev/sd?
[root@localhost ~ ]# fdisk /dev/sdc
  m
  n
  p
  1
  1
  enter
  w

[root@localhost ~ ]#
mkdir -p /bkp/archive
chown postgres:postgres /bkp/archive/
[root@localhost ~ ]# mkfs.ext4 /dev/sdc1
[root@localhost ~ ]# mount /dev/sdc1 /bkp/

[root@localhost ~ ]# nano /etc/fstab
/dev/sdc1 /bkp/ ext4 defaults 0 0







[root@localhost ~ ]# lsblk
[root@localhost ~ ]# ls -l /dev/sd?
[root@localhost ~ ]# fdisk /dev/sdb
 m
 n
 p
 1
 1
 enter
 w

[root@localhost ~ ]#
mkdir -p /var/lib/postgresql/
chown postgres:postgres /var/lib/postgresql/
[root@localhost ~ ]# mkfs.ext4 /dev/sdb1
[root@localhost ~ ]# mount /dev/sdb1 /var/lib/postgresql/


[root@localhost ~ ]# nano /etc/fstab
/dev/sdb1 /var/lib/postgresql/ ext4 defaults 0 0





# resize2fs /dev/sdb1 size
# resize2fs /dev/sdb1 800G


growpart /dev/sdb 1
resize2fs /dev/sdb1

sudo dd iflag=direct if=/dev/oracleoci/oraclevdb of=/dev/null count=1
echo "1" | sudo tee /sys/class/block/`readlink /dev/oracleoci/oraclevdb | cut -d'/' -f 2`/device/rescan



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



PostgreSQL is an object-relational database management system (ORDBMS) based on POSTGRES,
Version 4.21
, developed at the University of California at Berkeley Computer Science Department.


The implementation of POSTGRES began in 1986.

e Berkeley POSTGRES project
officially ended with Version 4.2.


Postgres95 - 1994


PostgreSQL
By 1996, it became clear that the name “Postgres95” would not stand the test of time. We chose a new
name, PostgreSQL, to reflect the relationship between the original POSTGRES and the more recent
versions with SQL capability



POSTGRES
Postgres95
PostgreSQL



watch ls -lath


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
initdb




root# mkdir /usr/local/pgsql
root# chown postgres /usr/local/pgsql
root# su postgres
postgres$ /usr/lib/postgresql/13/bin/initdb -D /usr/local/pgsql/data



You can now start the database server using:
/usr/lib/postgresql/13/bin/pg_ctl -D /usr/local/pgsql -l logfile start

/usr/lib/postgresql/13/bin/pg_ctl -D /usr/local/pgsql -l logfile stop


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Starting the Database Server

The bare-bones way to start the server manually is just to invoke postgres directly, specifying the
location of the data directory with the -D option, for example:
$ postgres -D /usr/local/pgsql/data


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
How To Change IP Address on Linux


vi /etc/network/interfaces


systemctl restart networking


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
count tables in database

    postgres=# \pset border 2
    postgres=# \pset pager off
    postgres=# \timing

postgres=# select 'SELECT COUNT(*) ' || 'FROM ' || tablename || ';'   from pg_catalog.pg_tables;


Inst - dif tit
Adriano fabio de almeida



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
pgbench





PedroAkira1817#aPe2-

/lib/postgresql/14/bin
createdb pgbench
pgbench -i pgbench

  pgbench -c 50 -j 2 -t 100 pgbench






ALTER SYSTEM SET max_connections = '5000';

ALTER SYSTEM SET shared_buffers  = '1280MB';
ALTER SYSTEM SET wal_buffers  = '400MB';

show work_mem;
ALTER SYSTEM SET work_mem  = '6GB';

show maintenance_work_mem;
ALTER SYSTEM SET  maintenance_work_mem = '640MB';

show max_wal_size;
ALTER SYSTEM SET  max_wal_size = '20GB';

postgres=#  SELECT pg_reload_conf();
root@srv:~# systemctl restart postgresql.service





SELECT temp_files AS "Temporary files"
     , temp_bytes/1024 AS "Size of temporary files"
    FROM   pg_stat_database db;



select pg_stat_reset();




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Pg_base


Usage:
  pg_basebackup   [OPTION]....




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Part 19 - PostgreSQL Backup and Restore.



backup
pg_dump
pg_dumpall


Restore
pg_restore
psql

BACKUP FORMATS
1 Plain text (restore psql)

2 cutom archive (binary formart and restore via pg_restore)

3 tar archive (binary formart and restore via pg_restore)

4 Direcotry formart (dir formart, individual tables and master file having info restore via pg_restore)

Usage:
pg_dump [OPTION]... [DBNAME]

-Z, --compress =0-9
 compression level for compressed



 DEMO

 pg_dump --help

How to create POSTGRESQL DUMP FILE
***********************************

pg_dump database_name > database_name.sql
pg_dump database_name -f database_name.sql

daemon
pg_dump test1 -v > /postgres/backup/database_name.sql
pg_dump test1 -f /postgres/backup/database_name.sql


pg_dump test1 -f /postgres/backup/database_name.sql


pg_dump -v -U postgres -W -F t test1 -f /postgres/backup/database_name.tar


Restore demo
************
createdb -T templete0 restored_test1
psql restored_test1 < /postgres/backup/database_name.sql


BACKUP ALL database.
pg_dumpall > /postgres/backup/all.sql







------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

WAL Archiving in PostgreSQL
refe: https://www.youtube.com/watch?v=RxHmoFeKeU8


WAL Archiving

show wal_level;

show wal_keep_size;

postgres=# ALTER SYSTEM SET log_rotation_age = 1440; #minutes

postgres=#  SELECT pg_reload_conf();
root@srv:~# systemctl restart postgresql.service






archive


postgres=# show archive_mode;

postgres=# alter system set archive_mode to 'on';

sudo systemctl restart postgresql
systemctl restart postgresql.service

mkdir -p /bkp/archive
chown postgres:postgres /bkp/archive/

postgres=# alter system set archive_command to 'cp %p /bkp/archive/%f';

postgres=# select pg_reload_conf();

postgres=# select pg_switch_wal();


select name,context,setting,unit,reset_val from pg_settings where  name like '%wal%' or name like '%archive%' order by setting;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
pgmetric , open-source tool to collect and report info of running PostgreSQL
refe: https://www.youtube.com/watch?v=wRlk1EVmnnk&t=712s



https://pgmetrics.io/docs/install.html

INSTALL

wget https://github.com/rapidloop/pgmetrics/releases/download/v1.12.0/pgmetrics_1.12.0_linux_amd64.tar.gz
tar xvf pgmetrics_1.12.0_linux_amd64.tar.gz
cd pgmetrics_1.12.0_linux_amd64
./pgmetrics --help





------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

62 - pgbackrest : PostgreSQL backup and restore
refe: https://www.youtube.com/watch?v=_f7C1ebxc9o

What is pgbackRest
********************
pgbackRest is an open source backup tool taht creates physical backups with ehhancement as compated to pg_basebackup tool.
It can be used to perform an initial database copy for Steaming Replication by using an existing backup.
or we can use the delta option to rebuil an old standby server.

Backup Terminology
*********************

Full Backup:
  pgBackRest copies the entire contents of the database cluster to the backup.
  The first backup of the database cluster is always a Full Backup.
  pgbackRest is always able to restore a full backup directly.
  The full backup does not depend on any file outside of the full backup for consistency

Differential Backup:
  pgbackRest copies only these database cluster files that have changed since the last full backup.
  The advantage of a differential backup is that it requires less storeage than a full backup.
  For restore diffbkp + full backup must be valid.


Incremeltal Backup:
  pgbackRest copies only those database cluster files that have changed since the last backup last sucesull backup can be anyone as below.
      Incremental backup
      Differential bakcup
      Full backup












------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
postgres : pg collector ( data collector and analysis tool )









------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
About temp_tablespaces in PostgreSQL

https://blog.dbi-services.com/about-temp_tablespaces-in-postgresql/

\l+


select spcname AS "Name"
     , pg_catalog.pg_get_userbyid(spcowner) AS "Owner"
     , pg_catalog.pg_tablespace_location(oid) AS "Location"
  from pg_catalog.pg_tablespace
 where pg_catalog.pg_tablespace.spcname = 'pg_default'
 order by 1;


 If we create temporary objects, where will the files be created then?

create temporary table tmp1 ( a int, b text, c date );

select pg_relation_filepath('tmp1');
  pg_relation_filepath
 ----------------------
  base/12732/t3_16436
 (1 row)



 This is the standard directory of my “postgres” database:

 postgres@centos8pg:/home/postgres/ [pgdev] cd $PGDATA
 postgres@centos8pg:/u02/pgdata/DEV/ [pgdev] oid2name
 All databases:
     Oid  Database Name  Tablespace
 ----------------------------------
   12732       postgres  pg_default
   12731      template0  pg_default
       1      template1  pg_default
 postgres@centos8pg:/u02/pgdata/DEV/ [pgdev] ls -l base/12732/t3_16436
 -rw-------. 1 postgres postgres 0 Mar 12 18:17 base/12732/t3_16436


 So, by default, files required for temporary tables go to the same location as all the other files that make up the specific database. If we populate the temporary table the files will grow, of course:

 insert into tmp1 (a,b,c) select i,i::text,now() from generate_series(1,100) i;

 postgres=# \! ls -la $PGDATA/base/12732/t3_16436
 -rw-------. 1 postgres postgres 8192 Mar 12 18:41 /u02/pgdata/DEV/base/12732/t3_16436
 postgres=# insert into tmp1 (a,b,c) select i,i::text,now() from generate_series(1,1000) i;

 postgres=# \! ls -la $PGDATA/base/12732/t3_16436
 -rw-------. 1 postgres postgres 49152 Mar 12 18:42 /u02/pgdata/DEV/base/12732/t3_16436









------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BACKUP / RESTORE


list backups options/programs:
  -pgbackrest
  -barman
  -Pg_base backup -> pg_basebackup
  -Pg_dump    | pg_restore,psql
  -pg_dumpall | pg_restore,psql
















------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
pg_basebackup

pg_basebackup backup utility

backups of a running PostgreSQL database cluster
used both for point-in-time recovery
staring point for a long shipping or streaming replication standyby servers

Binary copy of the database cluter files
not for single database.

Only super/user having replication permission can take backup.
pg_hba.conf must explicitly permit the replication connection.
pg_basebackup can make a base backup for master / standby.

usege:

  pg_backup [OPTION]...



-backup includes all data directory / (T/s)Configuration files/third part files.
- tablespaces will in plain format at same unless the option --tablespace-mapping
- when tar format mode is used, it is the user's responsibility to unpack each tar file




DEMO:

sudo su - postgres
sudo systemclt status postgresql-14
psql
postgres=# \du
           \l

Prerequisites
*******************************

Role creation to perform the backup.
            postgres=# CREATE USER replication_user WITH REPLICATION ENCRYTED PASSWORD 'password';

adding creted user in pg_hba.conf file ti to allow backup
          echo "host replication replication_user 192.168.1.51/32 md5" >> $PGDATA/pg_hba.conf
          view

/etc/postgresql/[VERSION]/main/pg_hba.conf
SHOW config_file;

get the chagnes into effect through a reload.
          # sudo su - postgres
          $ psql -c "select pg_realod_conf()"


pg_basebackup --help


pg_basebackup -U postgres -D /postgresql/pg_basebackup/

chown postgres /postgresql/pg_basebackup/
chgrp postgres /postgresql/pg_basebackup/

ls -lath /postgresql/pg_basebackup/
ls -lath /var/lib/postgresql/14/main/


create a backup of the local server with one compressed tar file for each tablespace, and store it in the directory backup,
  showing a progress report while running:
        pg_basebackup -v -U postgres -D /postgresql/pg_basebackup/ -Ft -z -P
        pg_basebackup -U postgres -D /postgresql/pg_basebackup/ -Ft -z -P

https://www.youtube.com/watch?v=lkN_cFx5O1s&t=534s
https://www.youtube.com/watch?v=QsSPwmm0-20
https://postgrespro.com/


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
barman












https://www.youtube.com/watch?v=isx-KYVvJQ8&t=242s
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
POSTGRESQL STRUCTURE



postgres=# \d+ pg_database
postgres=# select *  from pg_database where false;
SELECT datname, oid FROM pg_database;


postgres=# \d+ pg_class
postgres=# select *  from pg_class where false;
SELECT relname, oid FROM pg_class WHERE relname = 'pg_statistic'


SELECT relname, oid FROM pg_class
select 'SELECT COUNT(*) ' || 'FROM ' || tablename || ';'   from pg_catalog.pg_tables where schemaname ='public';

select 'SELECT pg_relation_filepath ' || '('||relname || ')' || ';'   from pg_class;

select 'SELECT pg_relation_filepath ' || '(' ||relname || ')' || ';'   from pg_class;
select concat('SELECT pg_relation_filepath', '(''', relname, ')') from pg_class;


select concat('SELECT pg_relation_filepath', '(''', relname, ')'', ';') from pg_class;



SELECT pg_relation_filepath ( pg_toast_1247_index;

SELECT pg_relation_filepath('pg_statistic');




select 'SELECT pg_relation_filepath ' || '(' ||relname || 'E)' || ';'   from pg_class;

SELECT replace('test string', 'st', '**');


select 'SELECT pg_relation_filepath ' || '(''' ||relname || E'\')' || ';'   from pg_class;




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
POSTGRESQL STRUCTURE 2.0


postgres=# \d+ pg_database
postgres=# select *  from pg_database where false;
SELECT datname, oid FROM pg_database;


postgres=# \d+ pg_class
postgres=# select *  from pg_class where false;
SELECT relname, oid FROM pg_class WHERE relname = 'pg_statistic'
SELECT relname, oid FROM pg_class;
SELECT * FROM pg_class limit 10;



select 'SELECT pg_relation_filepath ' || '(''' ||relname || E'\')' || ';'   from pg_class;
select 'SELECT pg_relation_filepath ' || E'(\'' ||relname || E'\')' || ';'   from pg_class;
select 'SELECT pg_relation_filepath ' || E'(\'' ||relname || E'\')' || ';'   from pg_class where reltuples > 0 and relispopulated = 'true';



SELECT pg_relation_filepath('pg_statistic');

vi /var/lib/postgresql/14/main/base/13726/2619
:%!xxd


# \pset format
# \H
# \x
# \pset pager off

# \x off;\pset format wrapped
# \x auto

sudo apt install binutils



createdb testdb_


--MONIT
--count of files in the pg_wal directory
SELECT COUNT(*) FROM pg_ls_dir('pg_wal') WHERE pg_ls_dir ~ '^[0-9A-F]{24}';


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------







-
