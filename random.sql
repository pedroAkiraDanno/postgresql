------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
postgresql




su - postgres
psql
\l
CREATE DATABASE test1;
postgres-# \c test3
test3-#





CREATE TABLE COMPANY1(
   NAME           TEXT    NOT NULL
);

\d
\d company



do
$do$
declare
     i int;
begin
for  i in 1..10000000
loop
            INSERT INTO COMPANY1 (NAME) SELECT MD5(random()::text);
end loop;
end;
$do$;


select * from COMPANY1;
--drop table COMPANY1;





--drop table test1;
CREATE  TEMPORARY  TABLE test1(
   id           int
);


INSERT INTO test1 (id) SELECT * FROM generate_series(1, 1000);






-------temporary table
create temp table test2 (
  id1  numeric,
  id2  numeric,
  id3  numeric,
  id4  numeric,
  id5  numeric,
  id6  numeric,
  id7  numeric,
  id8  numeric,
  id9  numeric,
  id10 numeric)
with (oids = false);

do
$do$
declare
     i int;
begin
for  i in 1..10000000
loop
    insert into test2  values (random(), i * random(), i / random(), i + random(), i * random(), i / random(), i + random(), i * random(), i / random(), i + random());
end loop;
end;
$do$;



-- return bytes
select pg_database_size('test3');
select (pg_database_size('test3')/1024/1024/1024) as database_size_gb;


--table size
psql test3
SELECT  pg_size_pretty (pg_relation_size('pg_temp_3.test2'));



\timing


select count(*) from pg_temp_3.test2;

select id1 from pg_temp_3.test2 order by id1 desc limit 1;
select id1 from pg_temp_3.test2 order by id1 asc limit 1;


select id1 from pg_temp_3.test2 where id1 = 0.99999908286722;
select id1 from pg_temp_3.test2 where id1 =  0.00000536893162461638;
    Time: 82842.507 ms (01:22.843)






------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
scheduler



su - postgres
psql
\l
CREATE DATABASE test1;
postgres-# \c test1
test3-#



--drop table test1;
CREATE    TABLE test1(
   id           int
);






psql -U postgres -d test3 -a -f myInsertFile.sql


mkdir /var/lib/postgresql/scripts
nano myInsertFile.sql
INSERT INTO test1 (id) SELECT * FROM generate_series(1, 10);
##commit;
#DROP TABLE test1 ;
#DELETE FROM test1;



crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 2 hours starting from midnight
#* 0,2,4,6,8,10,12,14,16,18,20,22 * * * myInsertFile.sql
# exec every 10 minutes starting from midnight
*/10 * * * *  psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/myInsertFile.sql


BEGIN;
INSERT INTO test1 (id) SELECT * FROM generate_series(1, 10);
ROLLBACK;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
test connection



vi  mySleepFile.sql
SELECT pg_sleep(1);



vim conn.sh

#!/bin/bash
for i in {1..1000}
do
        psql  &
        #psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/mySleepFile.sql

done

chmod +x conn.sh




crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 35 minutes starting from midnight
*/45 * * * *  /var/lib/postgresql/scripts/conn.sh





------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Test scheduler autovacuum




psql -U postgres -d test3 -a -f & myInsertFile.sql



nano DeleteFile.sql
#vacuum test1;
#UPDATE test1 set id = id +1 where id = 1;
UPDATE test1 set id = id +1;
DELETE FROM test1;
#DROP TABLE test1 ;




crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 35 minutes starting from midnight
*/30 * * * *  psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/myDeleteFile.sql









------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Test scheduler SELECT


psql -U postgres -d test1 -a -f mySelectFile.sql



nano SelectFile.sql
\timing
SELECT DISTINCT *  from test1 order by id limit 10;
#SELECT COUNT(*)  from test1;



crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 1 hour starting from midnight
* 1 * * *  psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/mySelectFile.sql



crontab -l





------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Test scheduler INDEX



psql -U postgres -d test1 -a -f myIndexFile.sql



nano IndexFile.sql
DROP INDEX foo_idx;
CREATE INDEX foo_idx ON  test1(id);



crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 20 minutes starting from midnight
*/30 * * * *  psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/myIndexFile.sql


crontab -l






------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Test scheduler TEMP



psql -U postgres -d test1 -a -f TempFile.sql


nano CreateTempFile.sql

CREATE TABLE COMPANY1(
   NAME           TEXT    NOT NULL
);

create temp table test2 (
  id1  numeric,
  id2  numeric,
  id3  numeric,
  id4  numeric,
  id5  numeric,
  id6  numeric,
  id7  numeric,
  id8  numeric,
  id9  numeric,
  id10 numeric)
with (oids = false);


\d
\d company


nano TempFile.sql
do
$do$
declare
     i int;
begin
for  i in 1..10000000
loop
            INSERT INTO COMPANY1 (NAME) SELECT MD5(random()::text);
end loop;
end;
$do$;

do
$do$
declare
     i int;
begin
for  i in 1..10000000
loop
    insert into test2  values (random(), i * random(), i / random(), i + random(), i * random(), i / random(), i + random(), i * random(), i / random(), i + random());
end loop;
end;
$do$;


do
$do$
declare
     i int;
begin
for  i in 1..10
loop
            INSERT INTO COMPANY1 (NAME) SELECT sha512('random()::text');
end loop;
end;
$do$;



crontab -e


# Example of job definition:
# .—————- minute (0 – 59)
# | .————- hour (0 – 23)
# | | .———- day of month (1 – 31)
# | | | .——- month (1 – 12) OR jan,feb,mar,apr …
# | | | | .—- day of week (0 – 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * command to be executed


# exec every 20 minutes starting from midnight
*/30 * * * *  psql -U postgres -d test1 -a -f /var/lib/postgresql/scripts/TempFile.sql


crontab -l



























------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------










-
