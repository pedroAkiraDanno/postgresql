
/*
------------------------------------------------------------------------------------------------------------------------------------------------------------------

INDEX 








------------------------------------------------------------------------------------------------------------------------------------------------------------------



An Overview of PostgreSQL Indexes


Types Of Indexes 
PostgreSQL server provides following types of indexes, which each uses a different algorithm: 
    B-tree
    Hash
    GiST
    SP-GiST
    GIN
    BRIN


1. B-tree index 
The most common and widely used index type is the B-tree index. This is the default index type for the CREATE INDEX command, unless you explicitly mention the type during index creation. 

If the indexed column is used to perform the comparison by using comparison operators such as <, <=, =, >=, and >, then the  Postgres optimizer uses the index created by the B-tree option for the specified column.


 

Tips
1. Indexes add overhead to the database system as a whole, so they should be used sensibly.

For Example: The INSERT and UPDATE statements take more time on tables having indexes, whereas the SELECT statements become fast on those tables. The reason is that while doing INSERT or UPDATE, a database needs to insert or update the index values as well.

2. You may need to run the ANALYZE command regularly to update statistics to allow the query planner to update the decisions planner.

3. You need to maintain the index properly so that it will not get bloated. You can REINDEX or you can also re-create the index concurrently.

4. A GIN index is expected to run slower than a B-tree because of the flexibility it provides.

5. In PostgreSQL versions prior to v10, Hash indexes were not WAL-logged, so they might need to be rebuilt after a database crash, if there were any unwritten changes.

Starting with v10, WAL-logging support was added to Hash indexes, which makes them crash-safe and replicable. 




REFERENCE: 
      https://www.enterprisedb.com/postgres-tutorials/overview-postgresql-indexes








------------------------------------------------------------------------------------------------------------------------------------------------------------------

TEST: 


#CREATE DATABASE AND TABLES
      create database test_indexing;

      \c test_indexing

      \timing 

      create table test_indexing(id serial, name text);



#INSERT 
      insert into test_indexing (name) select 'bob' from generate_series(1,250000000);
      insert into test_indexing (name) select 'ali' from generate_series(1,250000000);




 #Fast way to discover the row count of a table in PostgreSQL     
                  #SELECT COUNT(id) FROM test_indexing;
                        count
                        -----------
                        550000000
                        (1 row)

                        Time: 116158.435 ms (01:56.158)


                  SELECT reltuples AS estimate FROM pg_class where relname = 'test_indexing';

                  SELECT reltuples::bigint AS estimate
                  FROM   pg_class
                  WHERE  oid = 'test_indexing'::regclass;
                        estimate
                        -----------
                        550000000
                                          

                  SELECT 
                  schemaname
                  ,relname
                  ,n_live_tup AS EstimatedCount 
                  FROM pg_stat_user_tables 
                  ORDER BY n_live_tup DESC;






#SELECT WITHOUT INDEX AND EXPLAIN ANALYZE 
      select * from test_indexing where id = 2;
                  Time: 166623.397 ms (02:46.623)


      explain analyze select * from test_indexing where id = 2;
                                                                              QUERY PLAN
                  --------------------------------------------------------------------------------------------------------------------------------------
                  Gather  (cost=1000.00..4824075.35 rows=1 width=9) (actual time=29.247..206082.837 rows=1 loops=1)
                  Workers Planned: 4
                  Workers Launched: 4
                  ->  Parallel Seq Scan on test_indexing  (cost=0.00..4823075.25 rows=1 width=9) (actual time=164437.239..205592.036 rows=0 loops=5)
                        Filter: (id = 2)
                        Rows Removed by Filter: 110000000
                  Planning Time: 0.280 ms
                  JIT:
                  Functions: 10
                  Options: Inlining true, Optimization true, Expressions true, Deforming true
                  Timing: Generation 2.259 ms, Inlining 488.215 ms, Optimization 38.550 ms, Emission 36.293 ms, Total 565.317 ms
                  Execution Time: 206083.500 ms
                  (12 rows)




#CREATE INDEX 
      create index idx_id on test_indexing (id);
            Time: 1103230.374 ms (18:23.230)


#SELECT WITH INDEX AND EXPLAIN ANALYZE 
      select * from test_indexing where id = 2;
            Time: 99.321 ms
            Time: 0.839 ms

      explain analyze select * from test_indexing where id = 2;
                                                                  QUERY PLAN
            ----------------------------------------------------------------------------------------------------------------------
            Index Scan using idx_id on test_indexing  (cost=0.57..2.79 rows=1 width=9) (actual time=0.033..0.034 rows=1 loops=1)
            Index Cond: (id = 2)
            Planning Time: 0.169 ms
            Execution Time: 0.063 ms
            (4 rows)

            Time: 35.067 ms
     


#INFO TABLE AND INDEX 
      \di+
                                                      List of relations
            Schema |  Name  | Type  |  Owner   |     Table     | Persistence | Access method | Size  | Description
            --------+--------+-------+----------+---------------+-------------+---------------+-------+-------------
            public | idx_id | index | postgres | test_indexing | permanent   | btree         | 12 GB |


      \dt+
                                          List of relations
      Schema |     Name      | Type  |  Owner   | Persistence | Access method | Size  | Description
      --------+---------------+-------+----------+-------------+---------------+-------+-------------
      public | test_indexing | table | postgres | permanent   | heap          | 23 GB |


#VACUUM 
      #test_indexing=# vacuum;
            VACUUM
            Time: 700761.124 ms (11:40.761)







#DROP INDEX AND TABLE
      DROP INDEX idx_id;
      #DROP TABLE test_indexing;






------------------------------------------------------------------------------------------------------------------------------------------------------------------











REFERENCE: 
      https://www.enterprisedb.com/postgres-tutorials/overview-postgresql-indexes
      https://www.youtube.com/watch?v=-qNSXK7s7_w                                             Database Indexing Explained (with PostgreSQL)
      https://medium.com/geekculture/indexing-in-postgres-db-4cf502ce1b4e
      https://www.youtube.com/watch?v=bpX2zRM5OJg                                             PostgreSQL : PostgreSQL Indexes
      https://www.youtube.com/watch?v=HAn1xu6_SW0                                             A talk about indexes
      https://www.youtube.com/watch?v=clrtT_4WBAw                                             PostgreSQL Indexing : How, why, and when.
      https://www.youtube.com/watch?v=n5-xEEQFqPY                                             B-tree indexes - learn more about the heart of PostgreSQL
















------------------------------------------------------------------------------------------------------------------------------------------------------------------


*/














