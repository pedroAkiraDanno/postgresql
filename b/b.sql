
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PostgreSQL Backup 



[root@server1 ~]# ps auxw |  grep postgres | grep -- -D
	/usr/lib/postgresql/13/bin/

postgres@srv:~$ pg_dump --help




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PostgreSQL Backup and Restore.

ref: https://www.youtube.com/watch?v=43MgeBsc9IM



PostgreSQL Backup Utility 

backup 
	pg_dump
	pg_dumpall

restore
	pg_restore
	psql


backup formats 
1) plain text (restore psql)
2) custom archive (binary format and restore via pg_restore)
3) tar archive (binary format and restore via pg_restore)
4) directory format (dir format, individual tables and master file having info restore via pg_restore)



PG_DUMP CONNECTION OPTION
Usage: 

pg_dump [OPTION]....[DBNAME]

-f, --file=FILENAME output file or directory name 
-F, --format=c|d|t|p output file format(custom,directory,tar,
plain text(default))
-j, --jobs=NUM 	use this many parallel jobs to dump 
-Z, --compress=0-9 compression level for compressed formats 
--lock-wait-timeout=TIMEOUT fail after waiting TIMEOUT for a table lock 
--no-sync 	do not wait for changes to be written safaly to disk 

-a, --data-only 	dump only the data, not the schema 
-b, --blobs 		include large objects in dump 
-c, --clean 		clean (drop) database objects recreating 
-C, --create 		include commands to create database in dump 
-n, --schema=PATTERN dump the specified schema(s) only

-t, --table=PATTERN dump the specified table(s) only
-s, --schema-only dump only the schema, no data

Connection options:

-d, --dbname=DBNAME database to dump 
-h, --host=HOSTNAME database server host or socket directory 



PG_RESTORE

pg_restore restores a postgres database from an archive created by pg_dump

Usage:

pg_restore [OPTION]...[FILE]
General options: 
-d, --dbname=NAME connect to database name 
-f, --file=FILENAME output file name (- for stdout)
-F, --format=c|d|t backup file format (should be automatic)
-l, --list print summarized TOC of archive
-v, --verbose verbose mode 

OPTIONS CONTROLLING THE RESTORE:
-a, --data-only  restore only the data, no schema 
-c, --clean 	(drop) database objects clean before 
-C, --create 	create target database 

-n, --schema=NAME 	restore only objects in this schema
-s, --schema-only 	restore only the schema, no data 
-t, --table=NAME 	restore named relation(table,view,etc)






DEMO ON BACKUP AND RESTORE 

postgres@srv:~$ pg_dump --help

postgres@srv:~$ pg_dumpall --help


how to create postgres dump file 
********************************

pg_dump database_name > database_name.sql 
pg_dump database_name -f database_name.sql 

DEMO
pg_dump -v test1 > /var/lib/postgresql/backup/test1.sql
pg_dump -v test1 -f /var/lib/postgresql/backup/test1.sql

pg_dump -v -U postgres -W -F t test1 > /var/lib/postgresql/backup/test1.tar


Restore demo 
***************
createdb -T template0 test1_new
psql test1_new < /var/lib/postgresql/backup/test1.sql


backup all databases. 
************************
pg_dumpall > /var/lib/postgresql/backup/all.sql


backup a specific postgres table

postgres# \c test1
\dt 

pg_dump -Fc --data-only -h<host> -p<port> -U<username> -W -d<database> -t<table> > /var/lib/postgresql/backup/filename.dump

pg_dump -Fc --data-only  -U postgres -W -d test1 -t test1 > /var/lib/postgresql/backup/table_test1.dump


Restore table data 
Before you restore need to connect to the database and truncate the table to clear all the data: 

psql -h<host> -p<port> -U<username> -W -d<database>
TRUNCATE <table> RESTART IDENTITY;
\q
that will connect to the database, truncate the table and then disconnect again. After which you can import the data into the empty table: 

pg_restore --data-only -h<host> -p<port> -U<username> -W -d<database> -t<table>  /var/lib/postgresql/backup/filename.dump





------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



















