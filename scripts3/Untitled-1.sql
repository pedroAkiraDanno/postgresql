




CREATE TABLE t_person (
    id serial,
    gender int,
    data char(40)
);

INSERT INTO t_person (gender, data)
    SELECT x % 2 + 1, 'data'
    FROM generate_series(1,5000000) AS x;


SELECT name, count(*)
    from t_gender as a, t_person AS b 
    where a.id = b.gender 
    GROUP BY 1;
    



